var searchData=
[
  ['gack_5frx',['gack_rx',['../classTHub.html#a3d0c38ac47d196c0e94e03a46a6f49ec',1,'THub']]],
  ['gack_5frx_5flocal',['gack_rx_local',['../classTGITile.html#afa7205db90733def990690aea8745e2f',1,'TGITile']]],
  ['gack_5ftx',['gack_tx',['../classTHub.html#a229e7f537651974a04186b231ebc417b',1,'THub']]],
  ['gack_5ftx_5flocal',['gack_tx_local',['../classTGITile.html#a5379c02b8c55a319d855e5cfbd3bb64e',1,'TGITile']]],
  ['garbiter',['garbiter',['../classTNoC.html#a0bf74b2ef4a7b941f92a79b7a788dc68',1,'TNoC']]],
  ['gateway_5fmap',['gateway_map',['../classTGatewayInterface.html#ae413eea61011c30cb45f8c1d9136bbc1',1,'TGatewayInterface']]],
  ['gatewayinterface_2ecpp',['gatewayinterface.cpp',['../gatewayinterface_8cpp.html',1,'']]],
  ['gatewayinterface_2eh',['gatewayinterface.h',['../gatewayinterface_8h.html',1,'']]],
  ['getaveragedelay',['getAverageDelay',['../classTGlobalStats.html#a3d75599ed61620ac960538608bdaa9a1',1,'TGlobalStats::getAverageDelay()'],['../classTGlobalStats.html#a423c151912d6cc086e650ab49b3a421d',1,'TGlobalStats::getAverageDelay(const int src_id, const int dst_id)'],['../classTStats.html#ad7e8d54120749d4b8bd31849328c12dc',1,'TStats::getAverageDelay(const int src_id)'],['../classTStats.html#ad0cf2e56bc33fd5ee7499d4e4072f81c',1,'TStats::getAverageDelay()']]],
  ['getaveragethroughput',['getAverageThroughput',['../classTGlobalStats.html#af4e1327859df7d8bed2e7a92c2c04fa5',1,'TGlobalStats::getAverageThroughput()'],['../classTGlobalStats.html#a880b33faf9e4e4187065e5d7be1e2ea1',1,'TGlobalStats::getAverageThroughput(const int src_id, const int dst_id)'],['../classTStats.html#a82767fc04df12f12e59901be82b7b0aa',1,'TStats::getAverageThroughput(const int src_id)'],['../classTStats.html#ace25eecd6c3a6e198a21f16179daebfd',1,'TStats::getAverageThroughput()']]],
  ['getbit',['getBit',['../classTProcessingElement.html#a7e2c3fb5ad1f829dff3f12550332e44e',1,'TProcessingElement']]],
  ['getcommunicationenergy',['getCommunicationEnergy',['../classTStats.html#accca6e663fed1afe32dc2620c2d27019',1,'TStats']]],
  ['getcurrentfreeslots',['getCurrentFreeSlots',['../classTBuffer.html#a0440c486123f972484684b8cec251f98',1,'TBuffer']]],
  ['getmaxbuffersize',['GetMaxBufferSize',['../classTBuffer.html#a081ba1252d20ec4fd5f225a65dab5e33',1,'TBuffer']]],
  ['getmaxdelay',['getMaxDelay',['../classTGlobalStats.html#a61a64e2b94d047aa9fb4f390397b6554',1,'TGlobalStats::getMaxDelay()'],['../classTGlobalStats.html#a1c1b7348b68d32d2730f6dd55fc07e07',1,'TGlobalStats::getMaxDelay(const int node_id)'],['../classTGlobalStats.html#a21335e3e87cce14e3a67ea89ac889f73',1,'TGlobalStats::getMaxDelay(const int src_id, const int dst_id)'],['../classTStats.html#ac0e8a5fad8d404c15b6b98103b48187b',1,'TStats::getMaxDelay(const int src_id)'],['../classTStats.html#af1713fa3d1d91c6070ba1feb60aa0c0e',1,'TStats::getMaxDelay()']]],
  ['getmaxdelaymtx',['getMaxDelayMtx',['../classTGlobalStats.html#aae31ed164fb91c868bb497dcec682b0b',1,'TGlobalStats']]],
  ['getpower',['getPower',['../classTGlobalStats.html#a1f8d870e1b052ef5b94bc52a7ea4dd82',1,'TGlobalStats::getPower()'],['../classTPower.html#a0e749c9705b69503db032b279e48755f',1,'TPower::getPower()']]],
  ['getpwrforward',['getPwrForward',['../classTPower.html#aac2a8021eaa9eb83ed4523c524ea050b',1,'TPower']]],
  ['getpwrincoming',['getPwrIncoming',['../classTPower.html#a2af7363a88846898dcdd5c1056b24c44',1,'TPower']]],
  ['getpwrrouting',['getPwrRouting',['../classTPower.html#adb3bea4bfdb91b5dd5507da499694ce8',1,'TPower']]],
  ['getpwrselection',['getPwrSelection',['../classTPower.html#a67c4be19754b5f2b3d2d763a5de4abf7',1,'TPower']]],
  ['getpwrstandby',['getPwrStandBy',['../classTPower.html#a581dd09b2377bd6718b1af690707602c',1,'TPower']]],
  ['getrandomsize',['getRandomSize',['../classTProcessingElement.html#a56fa206e7531d01b763b2cff0d6d6f08',1,'TProcessingElement']]],
  ['getreceivedflits',['getReceivedFlits',['../classTGlobalStats.html#a5c2c72993ecc61b61cd45c360d00150f',1,'TGlobalStats::getReceivedFlits()'],['../classTStats.html#a226c71d9320a2545c8508a46eb4411ee',1,'TStats::getReceivedFlits()']]],
  ['getreceivedpackets',['getReceivedPackets',['../classTGlobalStats.html#a7a6d11efe4d07b71b50256c8ce7e514c',1,'TGlobalStats::getReceivedPackets()'],['../classTStats.html#a1896150ad2251491c85238671a90c5fb',1,'TStats::getReceivedPackets()']]],
  ['getroutedflits',['getRoutedFlits',['../classTGatewayInterface.html#a976101a7cabd18c02e522457d206edb1',1,'TGatewayInterface']]],
  ['getroutedflitsmtx',['getRoutedFlitsMtx',['../classTGlobalStats.html#ad05948c268bf53dea9036be86007aa98',1,'TGlobalStats']]],
  ['getthroughput',['getThroughput',['../classTGlobalStats.html#a18625351ecb12f9ef89b93001c8b758a',1,'TGlobalStats']]],
  ['gettotalcommunications',['getTotalCommunications',['../classTStats.html#a5870a743dd7fbe8844e45e07a89f3ee5',1,'TStats']]],
  ['gflit_5frx',['gflit_rx',['../classTHub.html#ad4ae800862cfbcf61c1ae40ba37e5c00',1,'THub']]],
  ['gflit_5frx_5flocal',['gflit_rx_local',['../classTGITile.html#a201132299634c95f4bf823117a36fe44',1,'TGITile']]],
  ['gflit_5ftx',['gflit_tx',['../classTHub.html#a69a8703c9e1b623ed89b25859e5bf93c',1,'THub']]],
  ['gflit_5ftx_5flocal',['gflit_tx_local',['../classTGITile.html#ada24f8a0758cf08db2468e21a4325e32',1,'TGITile']]],
  ['gfree_5fslots',['gfree_slots',['../classTHub.html#ad72bf18fa0121302d54e9ca54f31f3e8',1,'THub']]],
  ['gfree_5fslots_5flocal',['gfree_slots_local',['../classTGITile.html#a4f9586cf827a649b7ec9de15dc28a97d',1,'TGITile']]],
  ['gfree_5fslots_5fneighbor',['gfree_slots_neighbor',['../classTHub.html#a1232d3ce7eb4ca44259b87cca4f4e14d',1,'THub']]],
  ['gfree_5fslots_5fneighbor_5flocal',['gfree_slots_neighbor_local',['../classTGITile.html#abe4bae79c394c7d3caf8d2bda6cbbc12',1,'TGITile']]],
  ['gi_5fbuffer_5fdepth',['GI_BUFFER_DEPTH',['../gatewayinterface_8h.html#a8db4cfe4fdc2f51f331f39250bae0929',1,'gatewayinterface.h']]],
  ['gir_5fdirections',['GIR_DIRECTIONS',['../GlobalTypeDefs_8h.html#ac8f4f2d8dcf449b5a6af4fc2cb09195b',1,'GlobalTypeDefs.h']]],
  ['git',['git',['../classTNoC.html#aaa879f95f775387a443049e07dcdfd26',1,'TNoC']]],
  ['gitile_2ecpp',['gitile.cpp',['../gitile_8cpp.html',1,'']]],
  ['gitile_2eh',['gitile.h',['../gitile_8h.html',1,'']]],
  ['global_5ftx_5findex',['GLOBAL_TX_INDEX',['../gatewayinterface_8h.html#af467471d90f449d6654cc62404084ad3',1,'gatewayinterface.h']]],
  ['globalstatus_2ecpp',['globalstatus.cpp',['../globalstatus_8cpp.html',1,'']]],
  ['globalstatus_2eh',['globalstatus.h',['../globalstatus_8h.html',1,'']]],
  ['globaltypedefs_2eh',['GlobalTypeDefs.h',['../GlobalTypeDefs_8h.html',1,'']]],
  ['greq_5frx',['greq_rx',['../classTHub.html#a5584d62777481ed3a7d71473774986a9',1,'THub']]],
  ['greq_5frx_5flocal',['greq_rx_local',['../classTGITile.html#aaee17972b60e42b7442c13ff2f75c01c',1,'TGITile']]],
  ['greq_5ftx',['greq_tx',['../classTHub.html#adcb37d403b992a6a30990cd18b89ed76',1,'THub']]],
  ['greq_5ftx_5flocal',['greq_tx_local',['../classTGITile.html#a072c11892b10ce9cd2b6f3b1102f65b5',1,'TGITile']]],
  ['gwi',['gwi',['../classTGITile.html#ad18147d4e69c133ae2eeb77e3642cfee',1,'TGITile']]],
  ['gwi_5fcurrent_5flevel_5frx_5freq',['gwi_current_level_rx_req',['../classTGlobalarbiter.html#ae89c47a7fc1cab3c561df0c5b73aaac5',1,'TGlobalarbiter']]],
  ['gwi_5fcurrent_5flevel_5ftx_5fch',['gwi_current_level_tx_ch',['../classTGlobalarbiter.html#a61784f6277c1fa4a40639b5b738d891e',1,'TGlobalarbiter']]]
];
