var searchData=
[
  ['req_5frx',['req_rx',['../classTGatewayInterface.html#addaab6f48e772de61894556456ee8310',1,'TGatewayInterface::req_rx()'],['../classTHub.html#a187a68e0f285489a1fdeb27decd97c96',1,'THub::req_rx()'],['../classTProcessingElement.html#a9bb86815b191ff01d9ace5a93ddf0d84',1,'TProcessingElement::req_rx()']]],
  ['req_5frx_5farbiter_5fch',['req_rx_arbiter_ch',['../classTGlobalarbiter.html#a25409ac84e3ec5ccf17f27137d1f5f97',1,'TGlobalarbiter::req_rx_arbiter_ch()'],['../classTGatewayInterface.html#ac76a84be43311636273b9fafa4e34e20',1,'TGatewayInterface::req_rx_arbiter_ch()']]],
  ['req_5frx_5flocal',['req_rx_local',['../classTGITile.html#ac776ead2d6daca8b64f315dcf6d1029c',1,'TGITile']]],
  ['req_5fsource_5fid',['req_source_id',['../classTGatewayInterface.html#a2687efb6dc4401482a43350c3a774ce9',1,'TGatewayInterface']]],
  ['req_5ftx',['req_tx',['../classTGatewayInterface.html#a472694cc554c5b4b062e51ef35937355',1,'TGatewayInterface::req_tx()'],['../classTHub.html#a38c70d268b93a0874df1c808bbe6fd58',1,'THub::req_tx()'],['../classTProcessingElement.html#a902b903b85e22ffcc888ba06e723f233',1,'TProcessingElement::req_tx()']]],
  ['req_5ftx_5farbiter',['req_tx_arbiter',['../classTGlobalarbiter.html#ae2c4fc8605cd9432d75b35ab37fe43ac',1,'TGlobalarbiter::req_tx_arbiter()'],['../classTGatewayInterface.html#aaad718ee334b8aaa06bc6a6976fb012f',1,'TGatewayInterface::req_tx_arbiter()'],['../classTGITile.html#a6b572956fd480de81afa1fc8f62c7210',1,'TGITile::req_tx_arbiter()']]],
  ['req_5ftx_5farbiter_5fflit',['req_tx_arbiter_flit',['../classTGlobalarbiter.html#a120eb5a60ed3421d075a4ee4fe169492',1,'TGlobalarbiter::req_tx_arbiter_flit()'],['../classTGatewayInterface.html#acb9127766a3cf6f55b0b188e68bc70bb',1,'TGatewayInterface::req_tx_arbiter_flit()']]],
  ['req_5ftx_5farbiter_5fhf',['req_tx_arbiter_hf',['../classTGlobalarbiter.html#a56369aa3e60aef16b380df0a418e85d8',1,'TGlobalarbiter::req_tx_arbiter_hf()'],['../classTGatewayInterface.html#a881bffcf1d623291bf2c70530c81882e',1,'TGatewayInterface::req_tx_arbiter_hf()']]],
  ['req_5ftx_5farbiter_5fsignal',['req_tx_arbiter_signal',['../classTNoC.html#a3891fddc479d39c12c9a95514d091d31',1,'TNoC']]],
  ['req_5ftx_5flocal',['req_tx_local',['../classTGITile.html#a30abcdc9cb5a3c288b0a56b61db64345',1,'TGITile']]],
  ['req_5ftxc_5farbiter',['req_txc_arbiter',['../classTGlobalarbiter.html#ad9419494792b55703849ca88213e2962',1,'TGlobalarbiter::req_txc_arbiter()'],['../classTGatewayInterface.html#af7172cffec902e8f4f58e1b03898bcc7',1,'TGatewayInterface::req_txc_arbiter()']]],
  ['reservation_5fack',['reservation_ack',['../classTGatewayInterface.html#ae0772112621927920782e100529941c6',1,'TGatewayInterface']]],
  ['reservation_5freq',['reservation_req',['../classTGlobalarbiter.html#a3ead2e1bb228b8dc44ce896ba04a531e',1,'TGlobalarbiter::reservation_req()'],['../classTGatewayInterface.html#abedbe928fd0c90bb323c27e2c8bc1e74',1,'TGatewayInterface::reservation_req()']]],
  ['reserve_5freq_5fqueue',['reserve_req_queue',['../classTGlobalarbiter.html#aa8c7efe916a6eb6ee431d0860df23f2f',1,'TGlobalarbiter::reserve_req_queue()'],['../classTGatewayInterface.html#a7187f0c1e4843d1616e288d35851c06e',1,'TGatewayInterface::reserve_req_queue()']]],
  ['reset',['reset',['../classTGlobalarbiter.html#a6e141906e42afaff15e5f4ca649979d7',1,'TGlobalarbiter::reset()'],['../classTGatewayInterface.html#a56fab70533457b2ed28ce34133144be5',1,'TGatewayInterface::reset()'],['../classTGITile.html#a05bddbdf9e89758cb2704e835ad8792a',1,'TGITile::reset()'],['../classTHub.html#ac96455ed366d856b444e284bc774c5dd',1,'THub::reset()'],['../classTNoC.html#af85a7600b7d1829fcf789d4747baf88b',1,'TNoC::reset()'],['../classTProcessingElement.html#a68a713d45c7f95d7f1983c632484ed2d',1,'TProcessingElement::reset()']]],
  ['rnd_5fgenerator_5fseed',['rnd_generator_seed',['../structTGlobalParams.html#a678cd349d721a381928cb35af9530eef',1,'TGlobalParams']]],
  ['routed_5fflits',['routed_flits',['../classTGatewayInterface.html#a0937cb544c1b6f337191d69dd6b15667',1,'TGatewayInterface']]],
  ['routing_5falgorithm',['routing_algorithm',['../structTGlobalParams.html#a4803c86d6ef699069b0cfe22c353b2cc',1,'TGlobalParams']]],
  ['routing_5ftable_5ffilename',['routing_table_filename',['../structTGlobalParams.html#a42a7e63c7f9a0f1203e02c9c6ad3cbfa',1,'TGlobalParams']]]
];
