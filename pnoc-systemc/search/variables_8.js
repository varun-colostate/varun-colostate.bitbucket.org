var searchData=
[
  ['lambda',['lambda',['../gatewayinterface_8h.html#a1f81eae32e3f48289bb91f60c7892b8f',1,'lambda():&#160;main.cpp'],['../main_8cpp.html#a44128fcd59b7968c68e347a14ef8c558',1,'lambda():&#160;main.cpp']]],
  ['last_5freceived_5fflit_5ftime',['last_received_flit_time',['../structCommHistory.html#a7d17fc590a793819c5f0a94a3de9cefa',1,'CommHistory']]],
  ['local_5fdrained',['local_drained',['../classTGatewayInterface.html#a9ca33621211ec01440a9923716ec3947',1,'TGatewayInterface']]],
  ['local_5fid',['local_id',['../classTGatewayInterface.html#ac37797122cd1389398fce16dc32eb2a7',1,'TGatewayInterface::local_id()'],['../classTProcessingElement.html#a1c9be924147f3af5f08a2862f9799e2a',1,'TProcessingElement::local_id()']]],
  ['local_5fxid',['local_xid',['../classTGatewayInterface.html#a2eaec0102a437876ff74ef78dbcd7b60',1,'TGatewayInterface']]],
  ['local_5fyid',['local_yid',['../classTGatewayInterface.html#a5172edc8ee3fedd0ed88f061e8e2cf37',1,'TGatewayInterface']]]
];
