var searchData=
[
  ['nearest_5fgateway_5fid',['nearest_gateway_id',['../classTGatewayInterface.html#aec0a502e3f93f44dc2a5b832de372e42',1,'TGatewayInterface::nearest_gateway_id()'],['../structTFlit.html#aa95e24bececb4c4b4649fa8b8bd5bff1',1,'TFlit::nearest_gateway_id()'],['../structTRouteData.html#a585fa83659227016a01118c3fbf720d4',1,'TRouteData::nearest_gateway_id()']]],
  ['never_5ftransmit',['never_transmit',['../classTProcessingElement.html#a20f15fd38cbe0737892f4efcde0492f2',1,'TProcessingElement']]],
  ['newid2coord',['newid2Coord',['../GlobalTypeDefs_8h.html#a047c05cdf60e7999dad07ed06e84a4fc',1,'GlobalTypeDefs.h']]],
  ['nextflit',['nextFlit',['../classTProcessingElement.html#a50801e4f20d3b932b3edd9268d93fc34',1,'TProcessingElement']]],
  ['noc',['noc',['../classTGlobalStats.html#acd96de9a30fc1ae40d7932c048581f72',1,'TGlobalStats']]],
  ['noc_2ecpp',['noc.cpp',['../noc_8cpp.html',1,'']]],
  ['noc_2eh',['noc.h',['../noc_8h.html',1,'']]],
  ['not_5freserved',['NOT_RESERVED',['../GlobalTypeDefs_8h.html#ad9abb79690204d797077070c9372d9e4',1,'GlobalTypeDefs.h']]],
  ['not_5fvalid',['NOT_VALID',['../GlobalTypeDefs_8h.html#a72695d31382ba5334881ad3407393642',1,'GlobalTypeDefs.h']]]
];
