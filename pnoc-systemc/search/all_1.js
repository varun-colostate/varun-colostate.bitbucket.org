var searchData=
[
  ['buffer',['buffer',['../classTBuffer.html#aaa54ca2157b480b03d02bad60e0bbec6',1,'TBuffer']]],
  ['buffer_2ecpp',['buffer.cpp',['../buffer_8cpp.html',1,'']]],
  ['buffer_2eh',['buffer.h',['../buffer_8h.html',1,'']]],
  ['buffer_5fdepth',['buffer_depth',['../structTGlobalParams.html#a27e5bbd3d8a34d7e166cd9ec1e10d8c7',1,'TGlobalParams']]],
  ['buffer_5flocal',['buffer_local',['../classTGatewayInterface.html#afcab97b4e3743faf00528e1468e5c59e',1,'TGatewayInterface']]],
  ['buffermonitor',['bufferMonitor',['../classTGatewayInterface.html#a26ea53c7d77167c817dc51c8f54325e9',1,'TGatewayInterface']]],
  ['buildcrossbar',['buildCrossbar',['../classTNoC.html#ab665fe1edf505ede77769084f57f0f52',1,'TNoC']]]
];
