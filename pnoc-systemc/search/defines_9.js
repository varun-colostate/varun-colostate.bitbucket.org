var searchData=
[
  ['photonic_5fbuffer_5fdepth',['PHOTONIC_BUFFER_DEPTH',['../GlobalTypeDefs_8h.html#aa9ef9b4849ca3d8d6a1d01c404eb8150',1,'GlobalTypeDefs.h']]],
  ['platform_5fid',['PLATFORM_ID',['../CMakeCCompilerId_8c.html#adbc5372f40838899018fadbc89bd588b',1,'PLATFORM_ID():&#160;CMakeCCompilerId.c'],['../CMakeCXXCompilerId_8cpp.html#adbc5372f40838899018fadbc89bd588b',1,'PLATFORM_ID():&#160;CMakeCXXCompilerId.cpp']]],
  ['pri',['PRI',['../GlobalTypeDefs_8h.html#ac21c270bfe049dfdd259c110b2793d32',1,'GlobalTypeDefs.h']]],
  ['pwr_5fforward_5fflit',['PWR_FORWARD_FLIT',['../GlobalTypeDefs_8h.html#a401e6114e00c2fdeff5c19aa3212da22',1,'GlobalTypeDefs.h']]],
  ['pwr_5fincoming',['PWR_INCOMING',['../GlobalTypeDefs_8h.html#a2bc931a4c9b336cfa08d1b8341010f1b',1,'GlobalTypeDefs.h']]],
  ['pwr_5frouting_5fxy',['PWR_ROUTING_XY',['../GlobalTypeDefs_8h.html#ac38bf538915ae5fe03ceb5ae2b419a0e',1,'GlobalTypeDefs.h']]],
  ['pwr_5frouting_5fxyz',['PWR_ROUTING_XYZ',['../GlobalTypeDefs_8h.html#a5ede996adc89ef6433704fe4ddd6a323',1,'GlobalTypeDefs.h']]],
  ['pwr_5fsel_5fbuffer_5flevel',['PWR_SEL_BUFFER_LEVEL',['../GlobalTypeDefs_8h.html#a21e707a94addd46143c36cc3b9a93c44',1,'GlobalTypeDefs.h']]],
  ['pwr_5fsel_5fnop',['PWR_SEL_NOP',['../GlobalTypeDefs_8h.html#aa3c44d4ca7154d103fe622f21c802d10',1,'GlobalTypeDefs.h']]],
  ['pwr_5fsel_5frandom',['PWR_SEL_RANDOM',['../GlobalTypeDefs_8h.html#ad2474ae140d5edeef18f5f642358dc59',1,'GlobalTypeDefs.h']]],
  ['pwr_5fstandby',['PWR_STANDBY',['../GlobalTypeDefs_8h.html#ae5d38682992300b6db0930d07190eed5',1,'GlobalTypeDefs.h']]]
];
