var searchData=
[
  ['max_5fbuffer_5fsize',['max_buffer_size',['../classTBuffer.html#a0726fd5ab782d6aef8d259b5943a26d1',1,'TBuffer']]],
  ['max_5fpacket_5fsize',['max_packet_size',['../structTGlobalParams.html#a35e82396e29734ca0921f5b071eaa0c8',1,'TGlobalParams']]],
  ['max_5fvolume_5fto_5fbe_5fdrained',['max_volume_to_be_drained',['../structTGlobalParams.html#af102011a0897e257fd8a9d83da304eaa',1,'TGlobalParams']]],
  ['mesh_5fdim_5fx',['mesh_dim_x',['../structTGlobalParams.html#a6e285a57bcfc2932820e6b608671e94b',1,'TGlobalParams']]],
  ['mesh_5fdim_5fy',['mesh_dim_y',['../structTGlobalParams.html#a29ea3d167bcc482c66e550b7a6ca1518',1,'TGlobalParams']]],
  ['min_5fpacket_5fsize',['min_packet_size',['../structTGlobalParams.html#a1b5135d3c302097be464747066ef262a',1,'TGlobalParams']]],
  ['mwmr_5fchannel_5fno',['mwmr_channel_no',['../classTGlobalarbiter.html#a584f2ebeb14292df0d91c218caae18df',1,'TGlobalarbiter::mwmr_channel_no()'],['../classTGatewayInterface.html#a08697fa8f393cf0ffd1b7cb7fe536c16',1,'TGatewayInterface::mwmr_channel_no()']]],
  ['mwmr_5fdata_5frx',['mwmr_data_rx',['../classTGatewayInterface.html#a2d8761e905319cbf49c0baee8088bc6f',1,'TGatewayInterface']]],
  ['mwmr_5fdata_5ftx',['mwmr_data_tx',['../classTGatewayInterface.html#a7830362a1481581e179285f7dc01a24e',1,'TGatewayInterface']]]
];
