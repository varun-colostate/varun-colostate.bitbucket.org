var searchData=
[
  ['channel_5fstatus_5fneighbor',['channel_status_neighbor',['../structTNoP__data.html#a1ce30713bfa484ccef5ec18a118e5a3b',1,'TNoP_data']]],
  ['chist',['chist',['../classTStats.html#a0562ff433d74a7f37825801913252c30',1,'TStats']]],
  ['clock',['clock',['../classTGlobalarbiter.html#a06d4796b22989016081fbbcc10828010',1,'TGlobalarbiter::clock()'],['../classTGatewayInterface.html#a64759e62997b5b374aca440c2b947ab4',1,'TGatewayInterface::clock()'],['../classTGITile.html#a50141e1fa57c82b854ef27a7df46a116',1,'TGITile::clock()'],['../classTHub.html#aac2c3ba2f41d73f0a837fe7b9b0565ab',1,'THub::clock()'],['../classTNoC.html#a10a1d8b17b79bb1a2da914296c9170c5',1,'TNoC::clock()'],['../classTProcessingElement.html#a2024f531bed0b1f7c70b824e61ae4af9',1,'TProcessingElement::clock()']]],
  ['cross_5fbar',['cross_bar',['../classTNoC.html#a465b03880adf844a7565b4a93f56cb13',1,'TNoC']]],
  ['cross_5fbar_5fadditional',['cross_bar_additional',['../classTNoC.html#aff518e42690bfe57d6ff4fc20a56a30f',1,'TNoC']]],
  ['current_5fid',['current_id',['../structTRouteData.html#a276dbad06ac73facdcdc8b66fa8de64f',1,'TRouteData']]],
  ['current_5flevel_5frx',['current_level_rx',['../classTGatewayInterface.html#a302424b73f4c9724691c07a76ca641be',1,'TGatewayInterface::current_level_rx()'],['../classTProcessingElement.html#a8e6e1c3683adcf632215229799a52c74',1,'TProcessingElement::current_level_rx()']]],
  ['current_5flevel_5ftx',['current_level_tx',['../classTGatewayInterface.html#af4c0bc7ebae9bccace6b6b1cff38eab1',1,'TGatewayInterface::current_level_tx()'],['../classTProcessingElement.html#a4650c37b6c6121f48d1f1ad348eccfd5',1,'TProcessingElement::current_level_tx()']]]
];
