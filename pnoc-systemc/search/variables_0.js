var searchData=
[
  ['ack_5frx',['ack_rx',['../classTGatewayInterface.html#a6a941646d32aafac8c1223475a7f6f99',1,'TGatewayInterface::ack_rx()'],['../classTHub.html#aebdd79e05984aa09603b5b195be0d27b',1,'THub::ack_rx()'],['../classTProcessingElement.html#a0c0467d9c45113ae6ece4829c54b276d',1,'TProcessingElement::ack_rx()']]],
  ['ack_5frx_5farbiter_5fch',['ack_rx_arbiter_ch',['../classTGlobalarbiter.html#a3a1b2fcf198912dbcd027fca23f3c34f',1,'TGlobalarbiter::ack_rx_arbiter_ch()'],['../classTGatewayInterface.html#adaf60369064933cd198d590f165864b4',1,'TGatewayInterface::ack_rx_arbiter_ch()']]],
  ['ack_5frx_5flocal',['ack_rx_local',['../classTGITile.html#a4378d7b5c48083c523c4bc4d8c60450c',1,'TGITile']]],
  ['ack_5ftx',['ack_tx',['../classTGatewayInterface.html#a54e63324436d94c8e0e59f86d0b3152e',1,'TGatewayInterface::ack_tx()'],['../classTHub.html#a0619bde9aeb69252fb1dd0763e93ef89',1,'THub::ack_tx()'],['../classTProcessingElement.html#a36c0a5a22ae2b8425f5c9d97b17342cc',1,'TProcessingElement::ack_tx()']]],
  ['ack_5ftx_5farbiter',['ack_tx_arbiter',['../classTGlobalarbiter.html#a90ba5950477cb8912a19fd59c57b04c2',1,'TGlobalarbiter::ack_tx_arbiter()'],['../classTGatewayInterface.html#af835a30d6d9b3baa0fb9c26832801336',1,'TGatewayInterface::ack_tx_arbiter()'],['../classTGITile.html#a75012d78eebc6c0c205e2deab33b800f',1,'TGITile::ack_tx_arbiter()']]],
  ['ack_5ftx_5farbiter_5fhf',['ack_tx_arbiter_hf',['../classTGlobalarbiter.html#a7ffa55e66c20fafd0a481db84929eb32',1,'TGlobalarbiter::ack_tx_arbiter_hf()'],['../classTGatewayInterface.html#a88598c316bb0171c39269c2014f74951',1,'TGatewayInterface::ack_tx_arbiter_hf()']]],
  ['ack_5ftx_5farbiter_5fsignal',['ack_tx_arbiter_signal',['../classTNoC.html#ab4bf1a5b5f732e3cc014e740de529c2b',1,'TNoC']]],
  ['ack_5ftx_5flocal',['ack_tx_local',['../classTGITile.html#a85b396480b38d79ad8e6c4ca4ac30917',1,'TGITile']]],
  ['ack_5ftxc_5farbiter',['ack_txc_arbiter',['../classTGlobalarbiter.html#acebeb12ee14cf7dcd226e4bafde5d648',1,'TGlobalarbiter::ack_txc_arbiter()'],['../classTGatewayInterface.html#a5b70759b346de371a8b489e4e0e169a7',1,'TGatewayInterface::ack_txc_arbiter()'],['../classTGITile.html#a8cfc54c5ca2230dc87fcaeb8e27e0686',1,'TGITile::ack_txc_arbiter()']]],
  ['ack_5ftxc_5farbiter_5fsignal',['ack_txc_arbiter_signal',['../classTNoC.html#a90199aeef3f6b38fd73b8f1283189d81',1,'TNoC']]],
  ['ack_5ftxc_5fgwi',['ack_txc_gwi',['../classTGlobalarbiter.html#adc8bae4928072b5ae40c6615d3c52f4b',1,'TGlobalarbiter::ack_txc_gwi()'],['../classTGatewayInterface.html#a455b7097aed844cb1870c274d23293dc',1,'TGatewayInterface::ack_txc_gwi()']]],
  ['arbiter_5fcurrent_5flevel_5frx_5fch',['arbiter_current_level_rx_ch',['../classTGatewayInterface.html#a1f7daaab0f6e5d7e3e24b0b0eb9e2e3e',1,'TGatewayInterface']]],
  ['arbiter_5fcurrent_5flevel_5ftx',['arbiter_current_level_tx',['../classTGatewayInterface.html#a3968eb2906aaf4dd804c1a896304b4da',1,'TGatewayInterface']]],
  ['arbiter_5fcurrent_5flevel_5ftx_5freq',['arbiter_current_level_tx_req',['../classTGatewayInterface.html#af5c5e80fbbab8abf2f703418bb8cb64b',1,'TGatewayInterface']]],
  ['available',['available',['../structTChannelStatus.html#a0cadaf32e26c2f134c82a8c7d0668d20',1,'TChannelStatus']]]
];
