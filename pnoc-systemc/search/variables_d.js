var searchData=
[
  ['safe_5fto_5freserve',['safe_to_reserve',['../classTGatewayInterface.html#a2a6ebe24541d071302ded27b32df108a',1,'TGatewayInterface']]],
  ['safe_5fto_5fsend',['safe_to_send',['../classTGatewayInterface.html#a8f5527200c58c01ad18e6750db461ca5',1,'TGatewayInterface']]],
  ['selection_5fstrategy',['selection_strategy',['../structTGlobalParams.html#ac5b72eab1e93869ff1eb37b86dd068df',1,'TGlobalParams']]],
  ['sender_5fid',['sender_id',['../structTNoP__data.html#a1a633ef089e65dfc015f208c99519c22',1,'TNoP_data']]],
  ['sequence_5fno',['sequence_no',['../structTFlit.html#addad7a110091f260b97c602bdcf12238',1,'TFlit']]],
  ['simulation_5ftime',['simulation_time',['../structTGlobalParams.html#aa7907840a6ea7a191ad5d474d704ba0e',1,'TGlobalParams']]],
  ['size',['size',['../structTPacket.html#adc94c6233f92a3742d4bfbc089f3f1c6',1,'TPacket']]],
  ['src_5fid',['src_id',['../structTFlit.html#a82209bb6bf24e49cfd3e546dd925b98d',1,'TFlit::src_id()'],['../structTPacket.html#a1e15d4dc19d62ba14d002acef908dcd0',1,'TPacket::src_id()'],['../structTRouteData.html#a0b4fd1fe0bd1bde3b3482638d4e2c73f',1,'TRouteData::src_id()'],['../structCommHistory.html#ab4651d7286dc877a0445bb2b3c51ae5e',1,'CommHistory::src_id()']]],
  ['start_5ffrom_5fport',['start_from_port',['../classTGatewayInterface.html#a343f8c18fb61166fcd0d7d6218f61771',1,'TGatewayInterface']]],
  ['stats',['stats',['../classTGatewayInterface.html#a469bcd83904803a559e527da4fe6f20d',1,'TGatewayInterface']]],
  ['stats_5fwarm_5fup_5ftime',['stats_warm_up_time',['../structTGlobalParams.html#a9aa32fbb5a79f83c407a45e566bd4bbb',1,'TGlobalParams']]]
];
