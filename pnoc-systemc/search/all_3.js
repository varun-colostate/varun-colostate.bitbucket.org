var searchData=
[
  ['data',['data',['../structTPayload.html#a7185e9350664388d340ab5fc7c59ee5b',1,'TPayload']]],
  ['data_5fack',['data_ack',['../classTGatewayInterface.html#a5022e2ecccc5a179d086efc300539e8b',1,'TGatewayInterface']]],
  ['data_5fack_5fqueue',['data_ack_queue',['../classTGlobalarbiter.html#a8766ccb713ea1646c293eb85ebbd7379',1,'TGlobalarbiter::data_ack_queue()'],['../classTGatewayInterface.html#ac20f8c42ca1dadaf14c57f0eed9eb571',1,'TGatewayInterface::data_ack_queue()']]],
  ['dec',['DEC',['../CMakeCCompilerId_8c.html#ad1280362da42492bbc11aa78cbf776ad',1,'DEC():&#160;CMakeCCompilerId.c'],['../CMakeCXXCompilerId_8cpp.html#ad1280362da42492bbc11aa78cbf776ad',1,'DEC():&#160;CMakeCXXCompilerId.cpp']]],
  ['default_5fbuffer_5fdepth',['DEFAULT_BUFFER_DEPTH',['../GlobalTypeDefs_8h.html#a35df7be82e8071fe7b9991a4b34bf9e1',1,'GlobalTypeDefs.h']]],
  ['default_5fdetailed',['DEFAULT_DETAILED',['../GlobalTypeDefs_8h.html#a5862c3402154b6c926ddf9259d4e97bd',1,'GlobalTypeDefs.h']]],
  ['default_5fdyad_5fthreshold',['DEFAULT_DYAD_THRESHOLD',['../GlobalTypeDefs_8h.html#a0e51050db97f74d193b606365dff7521',1,'GlobalTypeDefs.h']]],
  ['default_5fmax_5fpacket_5fsize',['DEFAULT_MAX_PACKET_SIZE',['../GlobalTypeDefs_8h.html#a63bd578d16e9f0e2768327a693881d65',1,'GlobalTypeDefs.h']]],
  ['default_5fmax_5fvolume_5fto_5fbe_5fdrained',['DEFAULT_MAX_VOLUME_TO_BE_DRAINED',['../GlobalTypeDefs_8h.html#aa0de1b8ff37e7df9466f73f4f98f8736',1,'GlobalTypeDefs.h']]],
  ['default_5fmesh_5fdim_5fx',['DEFAULT_MESH_DIM_X',['../GlobalTypeDefs_8h.html#a095484398e5ab0f6279dfe4ea4de605e',1,'GlobalTypeDefs.h']]],
  ['default_5fmesh_5fdim_5fy',['DEFAULT_MESH_DIM_Y',['../GlobalTypeDefs_8h.html#a232dc6fdd9c060242a9bb38d00e9edb6',1,'GlobalTypeDefs.h']]],
  ['default_5fmin_5fpacket_5fsize',['DEFAULT_MIN_PACKET_SIZE',['../GlobalTypeDefs_8h.html#a537af71058af1470b0e23c0266f58df2',1,'GlobalTypeDefs.h']]],
  ['default_5fpacket_5finjection_5frate',['DEFAULT_PACKET_INJECTION_RATE',['../GlobalTypeDefs_8h.html#aeee2a26a08d88e3ff1eb908767237563',1,'GlobalTypeDefs.h']]],
  ['default_5fphotonic_5fnoc',['DEFAULT_PHOTONIC_NOC',['../GlobalTypeDefs_8h.html#a5089b27316dd5afddc71609d10f881d4',1,'GlobalTypeDefs.h']]],
  ['default_5fprobability_5fof_5fretransmission',['DEFAULT_PROBABILITY_OF_RETRANSMISSION',['../GlobalTypeDefs_8h.html#aee31b766decf84dc98530e9298134b30',1,'GlobalTypeDefs.h']]],
  ['default_5freset_5ftime',['DEFAULT_RESET_TIME',['../GlobalTypeDefs_8h.html#a7cc59c3cb0f0adc08fa37d9a12305985',1,'GlobalTypeDefs.h']]],
  ['default_5frouting_5falgorithm',['DEFAULT_ROUTING_ALGORITHM',['../GlobalTypeDefs_8h.html#a066119579f531459740a2fbcebac51f3',1,'GlobalTypeDefs.h']]],
  ['default_5frouting_5ftable_5ffilename',['DEFAULT_ROUTING_TABLE_FILENAME',['../GlobalTypeDefs_8h.html#afbc8453ab543f53fee86d0925fe9ea51',1,'GlobalTypeDefs.h']]],
  ['default_5fselection_5fstrategy',['DEFAULT_SELECTION_STRATEGY',['../GlobalTypeDefs_8h.html#a0f5d057a6bd10124adbe3e2cb593e093',1,'GlobalTypeDefs.h']]],
  ['default_5fsimulation_5ftime',['DEFAULT_SIMULATION_TIME',['../GlobalTypeDefs_8h.html#aa242d2c07eefb4ded84a90e4b3018550',1,'GlobalTypeDefs.h']]],
  ['default_5fstats_5fwarm_5fup_5ftime',['DEFAULT_STATS_WARM_UP_TIME',['../GlobalTypeDefs_8h.html#ae7fd1e60515423c9f43c8f12544abd4f',1,'GlobalTypeDefs.h']]],
  ['default_5ftrace_5ffilename',['DEFAULT_TRACE_FILENAME',['../GlobalTypeDefs_8h.html#a191e56ce9a56ce3b40af3afca95a24eb',1,'GlobalTypeDefs.h']]],
  ['default_5ftrace_5fmode',['DEFAULT_TRACE_MODE',['../GlobalTypeDefs_8h.html#a4b66e24ce76c4603a916dbdd373fb6d1',1,'GlobalTypeDefs.h']]],
  ['default_5ftraffic_5fdistribution',['DEFAULT_TRAFFIC_DISTRIBUTION',['../GlobalTypeDefs_8h.html#aad9a66e0acdfd2f9c5e82b8db8ce61fd',1,'GlobalTypeDefs.h']]],
  ['default_5ftraffic_5ftable_5ffilename',['DEFAULT_TRAFFIC_TABLE_FILENAME',['../GlobalTypeDefs_8h.html#a03fa75a6574de76b49f4ee47dbb5c362',1,'GlobalTypeDefs.h']]],
  ['default_5fverbose_5fmode',['DEFAULT_VERBOSE_MODE',['../GlobalTypeDefs_8h.html#aae724ffe77b5c6fc5147b4ac1a71c528',1,'GlobalTypeDefs.h']]],
  ['delays',['delays',['../structCommHistory.html#a63c53ef8d30af9c56df06f4431366567',1,'CommHistory']]],
  ['detailed',['detailed',['../structTGlobalParams.html#a5eae7cd6e9950cafc52e6bf74fafaa40',1,'TGlobalParams']]],
  ['dir_5fin',['dir_in',['../structTRouteData.html#a8f3aebefa2c7a8540c4efad7c7a84b5d',1,'TRouteData']]],
  ['direction2ilinkid',['direction2ILinkId',['../GlobalTypeDefs_8h.html#a1f24190c385c48a45d382d76c36f2e9f',1,'GlobalTypeDefs.h']]],
  ['directions',['DIRECTIONS',['../GlobalTypeDefs_8h.html#a3e3d4e9e6ca78f948635651ace96a76c',1,'GlobalTypeDefs.h']]],
  ['drained_5fvolume',['drained_volume',['../main_8cpp.html#afc62ba3cf1ad41233ab9e1819807dc63',1,'main.cpp']]],
  ['drop',['Drop',['../classTBuffer.html#ac0a76f8348a9806c1320d2325d1631f8',1,'TBuffer']]],
  ['dst_5fid',['dst_id',['../structTFlit.html#a9dd460c8fa964891ce8cc79f18baa0e6',1,'TFlit::dst_id()'],['../structTPacket.html#a5857b61d89c6244fe8a9adf4152e61fa',1,'TPacket::dst_id()'],['../structTRouteData.html#ad512e5540fb857192a65b6cecd3e3b4d',1,'TRouteData::dst_id()']]],
  ['dyad_5fthreshold',['dyad_threshold',['../structTGlobalParams.html#a0a701e72cd1b8ca237b02d11cc773a2e',1,'TGlobalParams']]]
];
