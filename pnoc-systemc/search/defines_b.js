var searchData=
[
  ['safe_5fto_5fsend',['SAFE_TO_SEND',['../GlobalTypeDefs_8h.html#a8ad716f32972a4449619b0fffb035c81',1,'GlobalTypeDefs.h']]],
  ['sel_5fbuffer_5flevel',['SEL_BUFFER_LEVEL',['../GlobalTypeDefs_8h.html#ab7ee082ca5890aa09e2249e1f1a3f6c8',1,'GlobalTypeDefs.h']]],
  ['sel_5fnop',['SEL_NOP',['../GlobalTypeDefs_8h.html#ac17caa924b3bacd9c56eb6621d779c25',1,'GlobalTypeDefs.h']]],
  ['sel_5frandom',['SEL_RANDOM',['../GlobalTypeDefs_8h.html#a88a8e41c96fbc95fc44f68035884728f',1,'GlobalTypeDefs.h']]],
  ['stringify',['STRINGIFY',['../CMakeCCompilerId_8c.html#a43e1cad902b6477bec893cb6430bd6c8',1,'STRINGIFY():&#160;CMakeCCompilerId.c'],['../CMakeCXXCompilerId_8cpp.html#a43e1cad902b6477bec893cb6430bd6c8',1,'STRINGIFY():&#160;CMakeCXXCompilerId.cpp']]],
  ['stringify_5fhelper',['STRINGIFY_HELPER',['../CMakeCCompilerId_8c.html#a2ae9b72bb13abaabfcf2ee0ba7d3fa1d',1,'STRINGIFY_HELPER():&#160;CMakeCCompilerId.c'],['../CMakeCXXCompilerId_8cpp.html#a2ae9b72bb13abaabfcf2ee0ba7d3fa1d',1,'STRINGIFY_HELPER():&#160;CMakeCXXCompilerId.cpp']]],
  ['swmr_5fsize',['SWMR_SIZE',['../GlobalTypeDefs_8h.html#aa808a343c8fd6166bfebecd37052fe4d',1,'GlobalTypeDefs.h']]]
];
