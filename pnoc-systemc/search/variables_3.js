var searchData=
[
  ['data',['data',['../structTPayload.html#a7185e9350664388d340ab5fc7c59ee5b',1,'TPayload']]],
  ['data_5fack',['data_ack',['../classTGatewayInterface.html#a5022e2ecccc5a179d086efc300539e8b',1,'TGatewayInterface']]],
  ['data_5fack_5fqueue',['data_ack_queue',['../classTGlobalarbiter.html#a8766ccb713ea1646c293eb85ebbd7379',1,'TGlobalarbiter::data_ack_queue()'],['../classTGatewayInterface.html#ac20f8c42ca1dadaf14c57f0eed9eb571',1,'TGatewayInterface::data_ack_queue()']]],
  ['delays',['delays',['../structCommHistory.html#a63c53ef8d30af9c56df06f4431366567',1,'CommHistory']]],
  ['detailed',['detailed',['../structTGlobalParams.html#a5eae7cd6e9950cafc52e6bf74fafaa40',1,'TGlobalParams']]],
  ['dir_5fin',['dir_in',['../structTRouteData.html#a8f3aebefa2c7a8540c4efad7c7a84b5d',1,'TRouteData']]],
  ['drained_5fvolume',['drained_volume',['../main_8cpp.html#afc62ba3cf1ad41233ab9e1819807dc63',1,'main.cpp']]],
  ['dst_5fid',['dst_id',['../structTFlit.html#a9dd460c8fa964891ce8cc79f18baa0e6',1,'TFlit::dst_id()'],['../structTPacket.html#a5857b61d89c6244fe8a9adf4152e61fa',1,'TPacket::dst_id()'],['../structTRouteData.html#ad512e5540fb857192a65b6cecd3e3b4d',1,'TRouteData::dst_id()']]],
  ['dyad_5fthreshold',['dyad_threshold',['../structTGlobalParams.html#a0a701e72cd1b8ca237b02d11cc773a2e',1,'TGlobalParams']]]
];
