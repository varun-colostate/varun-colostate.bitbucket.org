var searchData=
[
  ['sc_5fhas_5fprocess',['SC_HAS_PROCESS',['../classTGlobalarbiter.html#a113012b2a22f60e6a828a70194491e02',1,'TGlobalarbiter::SC_HAS_PROCESS()'],['../classTGatewayInterface.html#adc12c5c0a5d7317c1260689f5d1bba6b',1,'TGatewayInterface::SC_HAS_PROCESS()'],['../classTHub.html#a41a253a2cb3c8199d0c25229efffa4ca',1,'THub::SC_HAS_PROCESS()'],['../classTProcessingElement.html#a7befd1907ed0b6992438fa1cf707df18',1,'TProcessingElement::SC_HAS_PROCESS()']]],
  ['sc_5fmain',['sc_main',['../main_8cpp.html#a0227a065a94af492b3d8b5ea88a6d3e8',1,'main.cpp']]],
  ['sc_5ftrace',['sc_trace',['../GlobalTypeDefs_8h.html#aa6b2fafe7a4ceeaa195a3d253f0b2f9b',1,'sc_trace(sc_trace_file *&amp;tf, const TFlit &amp;flit, string &amp;name):&#160;GlobalTypeDefs.h'],['../GlobalTypeDefs_8h.html#a377493579533993e2a60c6e35d9e67a9',1,'sc_trace(sc_trace_file *&amp;tf, const TNoP_data &amp;NoP_data, string &amp;name):&#160;GlobalTypeDefs.h'],['../GlobalTypeDefs_8h.html#aa0ac5e1d2cf9e1d2d28c64d8f732475a',1,'sc_trace(sc_trace_file *&amp;tf, const TChannelStatus &amp;bs, string &amp;name):&#160;GlobalTypeDefs.h']]],
  ['searchcommhistory',['searchCommHistory',['../classTStats.html#a8f24f9ae4a07cab8f491020f25d2ae85',1,'TStats']]],
  ['searchnode',['searchNode',['../classTNoC.html#a8e7a2dd51d85ff9a3bb6a43b47e7eb07',1,'TNoC']]],
  ['selection',['Selection',['../classTPower.html#a0892162b6dc3bc519022969a96c569e8',1,'TPower']]],
  ['setbit',['setBit',['../classTProcessingElement.html#af8c4d53fb621f45e42adf015a5ccc016',1,'TProcessingElement']]],
  ['setmaxbuffersize',['SetMaxBufferSize',['../classTBuffer.html#adbb2f88a450590bb771fa5abce62fe6b',1,'TBuffer']]],
  ['showstats',['showStats',['../classTGlobalStats.html#a187d6f863614b74cd47c314961ba5295',1,'TGlobalStats::showStats()'],['../classTStats.html#a185bd479a4002f336841733ffdb66378',1,'TStats::showStats()']]],
  ['size',['Size',['../classTBuffer.html#a50183991c203347297c32a1773b2c91d',1,'TBuffer']]],
  ['standby',['Standby',['../classTPower.html#ae84a381bf30dc9de0d0c6919834e4b8c',1,'TPower']]]
];
